package core

type contextkey string
var WG_CTX_KEY contextkey

var HISTORYDIR_KEY = "historydir"
var TARGETDIR_KEY  = "targetdir"
var FILEWORKERS_KEY = "fileworkers"
var POLLFREQ_KEY = "pollfreq"