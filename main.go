package main

import (
	"fmt"
	"os"
	"gitlab.cern.ch/compute-accounting/accounting-transfer/cli"
)

func main() {
	if err := cli.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
