package cli

import (
	"github.com/spf13/cobra"
	"gitlab.cern.ch/compute-accounting/accounting-transfer/serve"
)

func init() {
	RootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the copier daemon",
	Long:  "Start the copier daemon",
	Run:    do_serve,
}

func do_serve(cmd *cobra.Command, args []string) {
	serve.Run()
}