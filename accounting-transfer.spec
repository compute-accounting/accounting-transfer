%global debug_package %{nil}

Summary:       Accounting transfer tool
Name:          accounting-transfer
Version:       0.1
Release:       1%{?dist}
License:       Apache2 license
Source:        %{name}-%{version}.tgz
Group:         Development/Libraries
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Prefix:        %{_prefix}
BuildRequires: golang
ExclusiveArch: x86_64
Vendor:        batch-operations@cern.ch 
Url:           https://gitlab.cern.ch/compute-accounting/accounting-transfer

%description
Accounting transfer tool

%prep
%setup -q

%build
mkdir -p work/src/gitlab.cern.ch/compute-accounting/accounting-transfer
export GOPATH=${PWD}/work
mv * work/src/gitlab.cern.ch/compute-accounting/accounting-transfer/ || true
cd work/src/gitlab.cern.ch/compute-accounting/accounting-transfer/
go build

%install
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/etc/accounting-transfer
install -p -m 0777 work/src/gitlab.cern.ch/compute-accounting/accounting-transfer/accounting-transfer %{buildroot}/usr/bin/accounting-transfer
install -p work/src/gitlab.cern.ch/compute-accounting/accounting-transfer/accounting-transfer.yaml %{buildroot}/etc/accounting-transfer/accounting-transfer.yaml

%clean
rm -rf $RPM_BUILD_ROOT

%files
/usr/bin/accounting-transfer
%config(noreplace) /etc/accounting-transfer/accounting-transfer.yaml

