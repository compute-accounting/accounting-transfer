package cli

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"fmt"
	"os"
	"gitlab.cern.ch/compute-accounting/accounting-transfer/core"
)

var RootCmd = &cobra.Command{
	Use:   "accounting-transfer",
	Short: "Accounting transfer is a copier tool to move per-job HTCondor accounting files to the right place in a shared file system.",
	Long: `Accounting transfer is a copier tool to move per-job HTCondor accounting files to the right place in a shared file system`,
}

var (
	cfgFile string
)


func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c","", "config file (default is /etc/accounting-transfer/accounting-transfer.yaml)")
	RootCmd.PersistentFlags().BoolP("debug", "d", false, "run in debug mode")
	viper.BindPFlag("debug", RootCmd.PersistentFlags().Lookup("debug"))

}

func initConfig() {

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {

		// Search config in currentdir, then site
		viper.AddConfigPath(".")
		viper.AddConfigPath("/etc/accounting-transfer/")

		viper.SetConfigName("accounting-transfer")
	}

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}

	// DON'T refresh live on config file change
	// viper.WatchConfig()

	// set sensible config defaults
	//core.SetDefaults()

	// setup logger
	core.SetupLogger()
}