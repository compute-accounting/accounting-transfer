package serve

import (
	"sync"
	"os"
	"log"
	"context"
	"gitlab.cern.ch/compute-accounting/accounting-transfer/core"
	"os/signal"
	"syscall"
	"github.com/spf13/viper"
)

var wg sync.WaitGroup

func Run() {
	hostname, _ := os.Hostname()
	log.Printf("[INFO] Serve started on %s", hostname)

	globalCtx, cancel := context.WithCancel(context.WithValue(context.Background(), core.WG_CTX_KEY, &wg))

	// transferjobs fanout
	tj := make(chan core.TransferJob)

	// handle events
	core.SetupHandlerLoop(globalCtx, tj)

	for i := 0; i < viper.GetInt(core.FILEWORKERS_KEY) ; i++ {
		core.StartFileWorker(globalCtx, tj)
	}

	// ordered shutdown
	wg.Add(1)
	go func() {
		<-globalCtx.Done()
		defer wg.Done()
	}()

	// block until done
	signaled := make(chan os.Signal)
	signal.Notify(signaled, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <- signaled:
		log.Printf("[DEBUG] Got local termination signal. Stopping.")
	}
	// ..then start shutting down
	cancel()
	log.Printf("[DEBUG] Waiting for handlers to shut down")
	wg.Wait()
	log.Printf("[INFO] Serve stopped on %s", hostname)
}