package core

import (
	"github.com/spf13/viper"
	"os"
	"github.com/hashicorp/logutils"
	"log"
	"context"
	"sync"
	"time"
	"path/filepath"
	"fmt"
	"io"
)

type TransferJob struct {
	FullFilename string
	Stat         os.FileInfo
}

func SetupLogger() {
	var loglevel string
	if viper.GetBool("debug") {
		loglevel = "DEBUG"
	} else {
		loglevel = "INFO"
	}
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(loglevel),
		Writer:   os.Stderr,
	}
	log.SetOutput(filter)
}

// Checks for new files every 60 minutes
func SetupHandlerLoop(ctx context.Context, transferjobs chan<- TransferJob) {

	wg := ctx.Value(WG_CTX_KEY).(*sync.WaitGroup)
	log.Printf("[DEBUG] Starting handler loop")

	// setup main control loop, checking every 60 minutes
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-time.After(time.Duration(viper.GetInt(POLLFREQ_KEY)) * time.Second):
				RunScan(transferjobs, ctx)
			case <-ctx.Done():
				return
			}
		}
	}()

}

// Scans the history dir for new files and sends them all for processing
// Ignores file with a mtime of less than 60 seconds
func RunScan(transferjobs chan<- TransferJob, ctx context.Context) {
	log.Printf("[INFO] Running history file scan.")
	history_dir := viper.GetString(HISTORYDIR_KEY)
	log.Printf("[DEBUG] Listing files in directory: %s", history_dir)

	pattern := filepath.Join(history_dir, "history.*")
	files, err := filepath.Glob(pattern)
	if err != nil {
		log.Printf("[ERROR] Error searching for files: %s", err)
		return
	}
	for _, f := range (files) {
		fileStat, err := os.Stat(f)
		if err != nil {
			log.Printf("[ERROR] Error stat'ing file <%s>: %s", f, err)
			continue
		}
		if time.Since(fileStat.ModTime()) < 60*time.Second {
			log.Printf("[INFO] Skipping new file <%s>. Get on next check.", f)
			continue
		}
		// check mark-file and silently ignore if already handled
		markDoneFile := fmt.Sprintf(".fetched.%s", filepath.Base(f))
		markDoneTarget := filepath.Join(filepath.Dir(f), markDoneFile)
		_, err = os.Stat(markDoneTarget)
		if err == nil {
			// i.e. mark-file already exists
			log.Printf("[DEBUG] Skipping done file <%s>", f)
			continue
		}

		// send it for processing
		tj := TransferJob{FullFilename: f, Stat: fileStat}
		select {
		case transferjobs <- tj:
			//
		case <-ctx.Done():
			return
		}
	}
	log.Printf("[DEBUG] Finished history file scan")
}

// Start a processing worker - probably just one in this case
// Listens for a new file processing event and runs the processing logic
func StartFileWorker(ctx context.Context, transferjobs <-chan TransferJob) {

	wg := ctx.Value(WG_CTX_KEY).(*sync.WaitGroup)

	log.Printf("[DEBUG] Starting file handler")
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case tj := <-transferjobs:
				doTransferJob(tj)
			}
		}
	}()
}

// File processing: copies the file to target directory, renames locally
func doTransferJob(tj TransferJob) {

	sourceFile := tj.FullFilename
	sourceSize := tj.Stat.Size()

	targetDir := viper.GetString(TARGETDIR_KEY)
	targetFile := filepath.Join(targetDir, tj.Stat.Name())

	log.Printf("[INFO] Copying %s to %s", sourceFile, targetFile)

	// check it's not there
	_, err := os.Stat(targetFile)
	if err == nil {
		log.Printf("[ERROR] Destination file already exists: %s", targetFile)
		return
	}

	// open src
	src, err := os.Open(sourceFile)
	if err != nil {
		log.Printf("[ERROR] Can't open source file for copy <%s>: %s", sourceFile, err)
		return
	}
	defer src.Close()

	// open dest
	dst, err := os.Create(targetFile)
	if err != nil {
		log.Printf("[ERROR] Can't open destination file for copy <%s>: %s", targetFile, err)
		return
	}
	defer dst.Close()

	// copy
	n, err := io.Copy(dst, src)
	if err != nil {
		log.Printf("[ERROR] Failed copy of <%s> to <%s>: %s", sourceFile, targetFile, err)
		return
	}
	// check it looked ok
	if n != sourceSize {
		log.Printf("[ERROR] Mis-copy copy of <%s> to <%s>", sourceFile, targetFile)
		return
	}

	// close dest and check
	err = dst.Close()
	if err != nil {
		log.Printf("[ERROR] Error closing dest file <%s>: %s", targetFile, err)
		return
	}

	// close source
	src.Close()

	// mark a dot file to say its done
	markDoneFile := fmt.Sprintf(".fetched.%s", filepath.Base(sourceFile))
	markDoneTarget := filepath.Join(filepath.Dir(sourceFile), markDoneFile)

	mdf, err := os.Create(markDoneTarget)
	if err != nil {
		log.Printf("[ERROR] Error creating mark file <%s>: %s", markDoneTarget, err)
		return
	}
	// close and check
	err = mdf.Close()
	if err != nil {
		log.Printf("[ERROR] Error creating mark file <%s>: %s", markDoneTarget, err)
		return
	}

}
