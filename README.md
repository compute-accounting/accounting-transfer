# Accounting file copier tool for HTCondor

This tool scans the HTCondor per-job history file directory and copies the files
into a shared filesystem, with a suitbaly durable directory structure.

After verifying the destination file is OK, it deletes the local copy to avoid the
schedd's or CE's disk filling up.

It is intended to run as a daemon from Systemd or Supervisord.
